class AddUserRoleIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :users_role_id, :integer, index: true
  end
end
