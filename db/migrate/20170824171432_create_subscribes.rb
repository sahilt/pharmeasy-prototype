class CreateSubscribes < ActiveRecord::Migration[5.0]
  def change
    create_table :subscribes do |t|
        t.references :subscription
        t.references :subscriber
        t.string :status, default: "inactive"
        t.timestamps
    end
  end
end
