class CreatePharmeasyFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :pharmeasy_files do |t|
        t.references :fileable, polymorphic: true, index: true
        t.string :file_name
        t.string :file_type
        t.timestamps
    end
  end
end