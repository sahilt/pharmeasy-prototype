class CreateUsersRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :users_roles do |t|
        t.string :name
        t.string :dashboard_prefix
        t.timestamps
    end
  end
end
