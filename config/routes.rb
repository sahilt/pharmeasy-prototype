Rails.application.routes.draw do
  Rails.application.routes.draw do
    devise_for :users, controllers: {
      sessions: 'users/sessions'
    }
  end
  namespace :users do
    get :dashboard
    get :search
    post :request_access
    post :approve_access
  end
  root to: 'users#dashboard'
  resources :prescriptions
end
