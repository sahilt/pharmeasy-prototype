class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  belongs_to :users_role, class_name: "Users::Role"
  has_many :prescriptions

  has_many :subscriber_subscribes, -> { where status: Subscribe::INACTIVE_STATUS }, foreign_key: :subscription_id, class_name: "Subscribe" 
  has_many :subscribers, through: :subscriber_subscribes, source: :subscriber

  has_many :subscription_subcribes, -> { where status: Subscribe::ACTIVE_STATUS }, foreign_key: :subscriber_id, class_name: "Subscribe"    
  has_many :subscriptions, through: :subscription_subcribes, source: :subscription


  before_validation :generate_uuid, on: :create, presence: true, uniqueness: true

  accepts_nested_attributes_for :prescriptions, allow_destroy: true

  USER_ROLE_ID_SEARCH_MAP = {1 => [2,3], 2 => [1], 3 => [1]}

  def generate_uuid
    uuid = "#{(0...8).map { rand(9) }.join}"
    while ::User.find_by(uuid: uuid).present? do
      uuid = "#{(0...8).map { rand(9) }.join}"
    end
    self.uuid = uuid
  end

  def search(search)
    if search
      self.class.all.where(:users_role_id=>USER_ROLE_ID_SEARCH_MAP[self.users_role_id]).where('email ILIKE ? OR uuid ILIKE ?', "#{search}", "#{search}")
    else
      []
    end
  end
end
