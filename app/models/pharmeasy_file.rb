class PharmeasyFile < ActiveRecord::Base
  belongs_to :fileable, polymorphic: true, optional: true
  mount_uploader :file_name, FileUploader
end
