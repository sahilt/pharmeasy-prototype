class Subscribe < ApplicationRecord
  belongs_to :subscriber, foreign_key: "subscriber_id", class_name: "User"
  belongs_to :subscription, foreign_key: "subscription_id", class_name: "User"
  ACTIVE_STATUS = "active"
  INACTIVE_STATUS = "inactive" # default status

  def approve
    self.update_attributes(:status=>ACTIVE_STATUS)
  end
end
