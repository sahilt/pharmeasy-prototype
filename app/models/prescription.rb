class Prescription < ApplicationRecord
  belongs_to :user
  has_many :pharmeasy_files, as: :fileable, :dependent => :destroy
  accepts_nested_attributes_for :pharmeasy_files, allow_destroy: true
end