class UsersController < ApplicationController
  before_action :authenticate_user!, :set_current_user

  def search
    @search_results = @current_user.search(params[:q])
  end

  def dashboard
    @subscriptions = @current_user.subscriptions
    @subscribers = @current_user.subscribers
  end

  def request_access
    if params[:user_uuid].present?
      subscription_id = ::User.find_by_uuid(params[:user_uuid]).id
      subs = Subscribe.find_or_create_by(:subscription_id=>subscription_id, :subscriber_id=>@current_user.id)
      render :json => {:message => "Succsfuly submitted request. Please go to home page. We are building this page"}, status: 200
    else
        render :json => {:message => "invalid user uuid"}, status: 400
    end
  end

  def approve_access
     if params[:user_uuid].present?
      subscriber_id = ::User.find_by_uuid(params[:user_uuid]).id
      subs = Subscribe.where(:subscription_id=>@current_user.id, :subscriber_id=>subscriber_id).last
      subs.approve
      render :json => {:message => "Succsfuly approved access. Please go to home page. We are building this page"}, status: 200
    else
        render :json => {:message => "invalid user uuid"}, status: 400
    end 
  end

  def set_current_user
    @current_user = current_user
  end
end